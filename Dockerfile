FROM microsoft/dotnet:2.1-sdk
WORKDIR /app
ADD . .

RUN dotnet publish docker.csproj -o publish

ENTRYPOINT ["dotnet", "./publish/docker.dll"]

